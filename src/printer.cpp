#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#include "int_converter.h"
#include "reader.h"


int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "not enough arguments" << std::endl;
        return 1;
    }
    IntConverter ic;
    char buf[ic.unit_size()];
    std::ifstream is(argv[1], std::ifstream::binary);
    is.read(buf, ic.unit_size());
    int count = ic.parse(buf);
    std::cout << count << std::endl;
    Reader<int> r(&ic, &is, count, count);
    for (int i = 0; i < count; i++) {
        std::cout << r.next() << std::endl;
    }
    is.close();
    return 0;
}
