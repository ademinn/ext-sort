#pragma once
#include "converter.h"


class IntConverter : public Converter <int> {
    public:
        unsigned int unit_size() const { return 4; }
        int parse(char buf[]) const;
        void serialize(int obj, char buf[]) const;
};
