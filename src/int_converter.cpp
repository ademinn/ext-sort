#include "int_converter.h"

#include <string.h>


int IntConverter::parse(char buf[]) const {
    int result;
    memcpy(&result, buf, unit_size());
    return result;
}


void IntConverter::serialize(int obj, char buf[]) const {
    memcpy(buf, &obj, unit_size());
}
