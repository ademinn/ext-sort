#include <string>
#include <stdlib.h>
#include <time.h>
#include <fstream>

#include "int_converter.h"
#include "generator.h"

void generate(const IntConverter* ic, const int count, const char* output) {
    char buf[ic->unit_size()];
    srand(time(NULL));
    std::ofstream os(output, std::ofstream::binary);
    ic->serialize(count, buf);
    os.write(buf, ic->unit_size());
    int value;
    for (int i = 0; i < count; i++) {
        value = rand();
        ic->serialize(value, buf);
        os.write(buf, ic->unit_size());
    }
    os.close();
}
