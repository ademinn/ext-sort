#pragma once

#include <fstream>
#include <vector>
#include "converter.h"
#include "int_converter.h"
#include "reader.h"


template <typename T>
class FileReader {
    public:
        FileReader(const Converter<T>* c, const char* input, const int buf_size=1024);
        ~FileReader() { delete _r; }

        T next() { return _r->next(); }
        bool has_data() { return _r->has_data(); }

        std::vector<T> read_all();
    private:
        const Converter<T>* _c;
        std::ifstream _is;
        Reader<T>* _r;
};


template <typename T>
FileReader<T>::FileReader(const Converter<T>* c, const char* input, const int buf_size)
    : _c(c), _is(input)
{
    IntConverter ic;
    char buf[ic.unit_size()];
    _is.read(buf, ic.unit_size());
    int stream_size = ic.parse(buf);
    _r = new Reader<T>(c, &_is, stream_size, buf_size);
}


template <typename T>
std::vector<T> FileReader<T>::read_all() {
    std::vector<T> result;
    while (has_data()) {
        result.push_back(next());
    }
    return result;
}
