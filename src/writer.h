#pragma once
#include <ostream>

#include "converter.h"


template <typename T>
class Writer {
    public:
        Writer(const Converter<T>* c, std::ostream* os, const int buf_size)
            : _c(c), _os(os), _buf_size(buf_size), _buf_byte_size(_buf_size * c->unit_size()),
            _buf(new char[_buf_byte_size]), _buf_count(0)
        {}

        void write(const T obj);

        void flush();

    private:
        const Converter<T>* _c;
        std::ostream* _os;

        const int _buf_size;
        const int _buf_byte_size;
        char* _buf;
        int _buf_count;
};


template <typename T>
void Writer<T>::write(const T obj) {
    _c->serialize(obj, _buf + (_buf_count * _c->unit_size()));
    _buf_count++;
    if (_buf_count >= _buf_size) {
        flush();
    }
}


template <typename T>
void Writer<T>::flush() {
    _os->write(_buf, _buf_count * _c->unit_size());
    _buf_count = 0;
}
