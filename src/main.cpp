#include <iostream>
#include <stdlib.h>
#include "int_converter.h"
#include "sort.h"


int main(int argc, char** argv) {
    if (argc < 5) {
        std::cerr << "not enough arguments" << std::endl;
        return 1;
    }
    int size = atoi(argv[1]);
    IntConverter c;
    sort(&c, size, argv[2], argv[3], argv[4]);
    return 0;
}
