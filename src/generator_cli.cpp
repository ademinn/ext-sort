#include <stdlib.h>
#include <iostream>

#include "int_converter.h"
#include "generator.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cerr << "not enough arguments" << std::endl;
        return 1;
    }
    int count = atoi(argv[1]);
    IntConverter ic;
    generate(&ic, count, argv[2]);
    return 0;
}
