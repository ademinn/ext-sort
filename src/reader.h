#pragma once
#include <istream>
#include <stdexcept>
#include <algorithm>
#include <iostream>

#include "converter.h"


class ReaderException : public std::runtime_error {
    public:
        ReaderException(const char* what)
            : std::runtime_error(what)
        {}
};


template <typename T>
class Reader {
    public:
        Reader(const Converter<T>* c, std::istream* is, const int stream_size, const int buf_size)
            : _c(c), _is(is), _stream_size(stream_size), _stream_count(0),
            _buf_size(buf_size), _buf_byte_size(buf_size * c->unit_size()),
            _buf(new char[_buf_byte_size]), _buf_count(buf_size)
        {}
        ~Reader() { delete[] _buf; }

        T next();
        bool has_data() const { 
            return (_stream_count < _stream_size) || (_buf_count < _buf_size); };

    private:
        const Converter<T>* _c;

        std::istream* _is;
        const int _stream_size;
        int _stream_count;

        int _buf_size;
        const int _buf_byte_size;
        char* _buf;
        int _buf_count;

};


template <typename T>
T Reader<T>::next() {
    if (_buf_count >= _buf_size) {
        if (!has_data()) {
            throw ReaderException("stream is empty");
        }
        int stream_elems = _stream_size - _stream_count;
        int stream_bytes = stream_elems * _c->unit_size();
        int read_bytes = _buf_byte_size;
        if (stream_bytes < _buf_byte_size) {
            read_bytes = stream_bytes;
            _buf_size = stream_elems;
            _stream_count += stream_elems;
        } else {
            _stream_count += _buf_size;
        }
        _is->read(_buf, read_bytes);
        _buf_count = 0;
    }
    T result = _c->parse(_buf + (_buf_count * _c->unit_size()));
    _buf_count++;
    return result;
}
