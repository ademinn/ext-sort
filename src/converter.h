#pragma once


template <typename T>
class Converter {
    public:
        virtual unsigned int unit_size() const = 0;
        virtual T parse(char buf[]) const = 0;
        virtual void serialize(T obj, char buf[]) const = 0;
};
