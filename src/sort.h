#pragma once
#include <fstream>
#include <algorithm>
#include <vector>
#include <iostream>

#include "converter.h"
#include "int_converter.h"
#include "reader.h"
#include "writer.h"


template <typename T>
void sort_blocks(const Converter<T>* c, const int size, const char* input, const char* tmp) {
    std::ifstream is(input);
    std::ofstream os(tmp);
    IntConverter ic;
    char buf[ic.unit_size()];
    is.read(buf, ic.unit_size());
    os.write(buf, ic.unit_size());
    int stream_size = ic.parse(buf);
    int stream_count = 0;
    std::vector<T> elems;
    int buf_size;
    while (stream_count < stream_size) {
        buf_size = std::min(size, stream_size - stream_count);
        stream_count += buf_size;
        elems.clear();
        elems.resize(buf_size);
        {
            Reader<T> r(c, &is, buf_size, buf_size);
            for (int i = 0; i < buf_size; i++) {
                T e = r.next();
                elems[i] = e;
            }
        }
        std::sort(elems.begin(), elems.end());
        {
            Writer<T> w(c, &os, buf_size);
            for (int i = 0; i < buf_size; i++) {
                w.write(elems[i]);
            }
        }
    }
    is.close();
    os.close();
}


template <typename T>
void merge_blocks(const Converter<T>* c, const int size, const char* tmp, const char* output) {
    std::ifstream is(tmp);
    std::ofstream os(output);
    IntConverter ic;
    char buf[ic.unit_size()];
    is.read(buf, ic.unit_size());
    os.write(buf, ic.unit_size());
    int stream_size = ic.parse(buf);
    int full_blocks = stream_size / size;
    int all_blocks = stream_size % size == 0 ? full_blocks : full_blocks + 1;
    int buf_size = size / (all_blocks + 1);
    std::vector<std::ifstream*> streams(full_blocks + 1);
    std::vector<Reader<T>*> readers(full_blocks + 1);
    for (int i = 0; i < full_blocks; i++) {
        std::ifstream* _is = new std::ifstream(tmp);
        _is->seekg((1 + size * i) * c->unit_size());
        streams[i] = _is;
        readers[i] = new Reader<T>(c, _is, size, buf_size);
    }
    if (full_blocks != all_blocks) {
        std::ifstream* _is = new std::ifstream(tmp);
        _is->seekg((1 + size * full_blocks) * c->unit_size());
        streams[full_blocks] = _is;
        readers[full_blocks] = new Reader<T>(c, _is, stream_size - full_blocks * size, buf_size);
    }
    Writer<T> writer(c, &os, buf_size);

    std::vector<T*>elems(all_blocks);
    for (int j = 0; j < all_blocks; j++) {
        elems[j] = new T(readers[j]->next());
    }

    T* min = NULL;
    int min_index;
    for (int i = 0; i < stream_size; i++) {
        for (int j = 0; j < all_blocks; j++) {
            if (elems[j] != NULL && (min == NULL || *min > *elems[j])) {
                min = elems[j];
                min_index = j;
            }
        }
        writer.write(*min);
        min = NULL;
        if (readers[min_index]->has_data()) {
            *elems[min_index] = readers[min_index]->next();
        } else {
            delete elems[min_index];
            elems[min_index] = NULL;
        }
    }
    writer.flush();
    std::cout << "sorted" << std::endl;
    for (int i = 0; i < all_blocks; i++) {
        delete readers[i];
        streams[i]->close();
        delete streams[i];
    }
    is.close();
    os.close();
}


template <typename T>
void sort(const Converter<T>* c, const int size, const char* input, const char* tmp, const char* output) {
    sort_blocks(c, size, input, tmp);
    merge_blocks(c, size, tmp, output);
}
