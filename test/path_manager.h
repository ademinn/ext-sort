#pragma once

#include <string>
#include <boost/filesystem.hpp>

#include <stdexcept>

class PathManagerException : public std::runtime_error
{
    public:
        PathManagerException(const char* what_arg)
            : std::runtime_error(what_arg)
        {}
};

class PathManager
{
    public:
        static void set_root(const std::string& root);
        static const boost::filesystem::path& get_root_path();
        static const std::string get_sub_path(const std::string& name);

    private: 
        boost::filesystem::path* _root_dir;

        PathManager() : _root_dir(nullptr) {}
        PathManager(const PathManager&);  
        PathManager& operator=(PathManager&);
        ~PathManager();

        static PathManager& getInstance() {
            static PathManager instance;
            return instance;
        }
};
