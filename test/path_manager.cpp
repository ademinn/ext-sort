#include "path_manager.h"
#include <iostream>

namespace fs = boost::filesystem;

void PathManager::set_root(const std::string& root_dir)
{
    PathManager& mgr = getInstance();
    if (mgr._root_dir)
    {
        throw PathManagerException("root has already been set");
    }
    mgr._root_dir = new fs::path(root_dir);
}

const fs::path& PathManager::get_root_path()
{
    PathManager& mgr = getInstance();
    if (!mgr._root_dir)
    {
        throw PathManagerException("root has not been set");
    }
    return *mgr._root_dir;
}

const std::string PathManager::get_sub_path(const std::string& name)
{
    const fs::path& root = PathManager::get_root_path();
    return (root / name).string();
}

PathManager::~PathManager()
{
    if (_root_dir)
    {
        delete _root_dir;
    }
}
