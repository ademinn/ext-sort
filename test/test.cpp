#include <boost/test/unit_test.hpp>
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <utility>

#include "int_converter.h"
#include "file_reader.h"
#include "generator.h"
#include "sort.h"
#include "path_manager.h"


struct IntConverterFixture {
    IntConverterFixture() {}

    IntConverter ic;
};


std::pair<std::vector<int>, std::vector<int>>
gen_sort(const IntConverter* ic, const std::string& name, const int size, const int buf_size) {
    std::string input(PathManager::get_sub_path(name + ".input"));
    std::string tmp(PathManager::get_sub_path(name + ".tmp"));
    std::string output(PathManager::get_sub_path(name + ".output"));
    generate(ic, size, input.c_str());
    sort(ic, buf_size, input.c_str(), tmp.c_str(), output.c_str());
    FileReader<int> orig(ic, input.c_str());
    std::vector<int> orig_array = orig.read_all();
    FileReader<int> sorted(ic, output.c_str());
    std::vector<int> sorted_array = sorted.read_all();
    return std::pair<std::vector<int>, std::vector<int>>(orig_array, sorted_array);
}


BOOST_FIXTURE_TEST_CASE(MultisetEqual, IntConverterFixture)
{
    std::pair<std::vector<int>, std::vector<int>> result = gen_sort(&ic, "multiset_equal", 10, 5);
    std::sort(result.first.begin(), result.first.end());
    std::sort(result.second.begin(), result.second.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(result.first.begin(), result.first.end(),
            result.second.begin(), result.second.end());
}


BOOST_FIXTURE_TEST_CASE(ValidateSort, IntConverterFixture)
{
    std::pair<std::vector<int>, std::vector<int>> result = gen_sort(&ic, "validate_sort", 10, 5);
    std::sort(result.first.begin(), result.first.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(result.first.begin(), result.first.end(),
            result.second.begin(), result.second.end());
}


BOOST_FIXTURE_TEST_CASE(NotAligned, IntConverterFixture)
{
    std::pair<std::vector<int>, std::vector<int>> result = gen_sort(&ic, "not_aligned", 12, 5);
    std::sort(result.first.begin(), result.first.end());
    BOOST_CHECK_EQUAL_COLLECTIONS(result.first.begin(), result.first.end(),
            result.second.begin(), result.second.end());
}


boost::unit_test::test_suite* init_unit_test_suite(int argc, char** const argv)
{
    if (argc < 2)
    {
        throw boost::unit_test::framework::setup_error(
            "root directory with test data not specified");
    }
    PathManager::set_root(argv[1]);
    return nullptr;
}
